const path = require('path')
const resolve = dir => {
  return path.join(__dirname, dir)
}
const port = 8000
module.exports = {

  publicPath: './',
  chainWebpack: config => {
    config.resolve.alias
      .set('_c', resolve('src/components')) // key,value自行定义，比如.set('@@', resolve('src/components'))
  },
  //配置跨域请求
  devServer: {
    headers: {
      'Access-Control-Allow-Origin': '*'
    },
    overlay: {
      warnings: false,
      errors: true
    },
    open: false, //是否自动打开浏览器
    https: false, //是否开启https
    hotOnly: true, // 热更新
    port,
    proxy: { // 配置跨域
      '/ria': {
        target: 'http://121.37.193.224:8080/framework', //请求接口域名 
        ws: true,
        secure: false,
        changOrigin: true, //是否允许跨越
        pathRewrite: {
          '^/ria': ''
        }
      }
    }
  }
}