import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import dataV from '@jiaminghi/data-view';
// import VueAwesomeSwiper from 'vue-awesome-swiper'
import axios from 'axios'
// import VueAxios from 'vue-axios'
import ElementUI from 'element-ui';
import * as echarts from "echarts";
// 引入全局css
import './assets/scss/style.scss';
// 按需引入vue-awesome图标
import Icon from 'vue-awesome/components/Icon';
import 'vue-awesome/icons/chart-bar.js';
import 'vue-awesome/icons/chart-area.js';
import 'vue-awesome/icons/chart-pie.js';
import 'vue-awesome/icons/chart-line.js';
import 'vue-awesome/icons/align-left.js';
import 'element-ui/lib/theme-chalk/index.css';
import 'weapp-cookie/dist/weapp-cookie'

import {message} from 'element-ui'
Vue.prototype.$message = message

import {Request} from './utils/https'
Vue.prototype.$Request=Request;

// import {post,fetch,patch,put} from './utils/http'
// //定义全局变量
// Vue.prototype.$post=post;
// Vue.prototype.$fetch=fetch;
// Vue.prototype.$patch=patch;
// Vue.prototype.$put=put;
//引入echart
//4.x 引用方式
// import echarts from 'echarts'
//5.x 引用方式为按需引用
//希望使用5.x版本的话,需要在package.json中更新版本号,并切换引用方式
import "echarts-liquidfill";
router.beforeEach((to, from, next) => {
  /* 路由发生变化修改页面title */
  if (to.meta.title) {
    document.title = to.meta.title
  }
  next()
})
// import 'swiper/dist/css/swiper.css';
// Vue.use(VueAwesomeSwiper)
// 全局注册
axios.defaults.withCredentials = true //跨域
Vue.prototype.$echarts = echarts
Vue.config.productionTip = true;
Vue.use(ElementUI);
Vue.component('icon', Icon);
Vue.use(dataV);
// axios.defaults.withCredentials = true
Vue.prototype.$axios = axios
Vue.config.productionTip = false
// Vue.use(VueAxios)

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
