import axios from 'axios';
// import { Message } from 'element-ui';

axios.defaults.timeout = 5000;
axios.defaults.baseURL ='';


//http response 拦截器
axios.interceptors.response.use(
  response => {
    if(response.data.errCode ==2){
    //   router.push({
    //     path:"/login",
    //     query:{redirect:router.currentRoute.fullPath}//从哪个页面跳转
    //   })
    }
    return response;
  },
  error => {
    return Promise.reject(error)
  }
)

/**
 * 封装post请求
 * @param requestURL
 * @param data
 * @returns {Promise}
 */

 export function Request(datas = {}) {
	// console.log('请求参数', datas)
	let requestURL = '/techcomp/ria/commonProcessor!commonMethod.action'
    let yy = new Date().getFullYear();
    let mm = new Date().getMonth()+1;
    let dd = new Date().getDate();
    let hh = new Date().getHours();
    let mf = new Date().getMinutes()<10 ? '0'+new Date().getMinutes() : new Date().getMinutes();
    let ss = new Date().getSeconds()<10 ? '0'+new Date().getSeconds() : new Date().getSeconds();
    let ReqTime = yy+''+mm+''+dd+hh+''+mf+''+ss;
	return new Promise((resolve, reject) => {
		const static_header = {
			"code": 0,
			"message": {
				"title": "",
				"detail": ""
			}
		}
        axios({
            headers: {
                'Accept-Language':'zh-CN,zh;q=0.9',  
                'Content-Type': 'application/json',
            },
            method: 'POST',
            url:`${requestURL}?ajaxRequest=true&t=${ReqTime}`,
            data: {
                header: static_header,
                body: datas
            },
        }).then((res) => {
            resolve(res);
        },err => {
            reject(err)
        });
    })
}
