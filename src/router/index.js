import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [{
  path: '/',
  name: 'index',
  meta: { hidden: true,title:"库点作业看板"},
  component: () => import('../views/InventoryStatistics/index.vue')
}, {
  path: '/home',
  name: 'home',
  meta: { hidden: true,title:"货主库存量展示"},
  component: () => import('../views/InventoryMap/home.vue')
}, {
  path: '/jcwms',
  name: 'jcwms',
  component: () => import('../views/jcwms/index.vue')
}]
const router = new VueRouter({
  // mode: "history",
  routes
})

export default router